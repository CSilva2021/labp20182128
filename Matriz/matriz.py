# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
# C L A S S E :     M A T R I Z          --> como lista de vetores
#
#                   M E M B R O S:        value       --> A lista que contem todos os vetores da matriz
#
#                   S E R V I Ç O S:    CRIA MATRIZ:            a) __init__             --> Operador da initialização de uma nova matriz
#                                       METÒDOS:                1) showMatrix           --> Representar a matriz no shell
#                                                               2) dim                  --> Dar a dimensão da matriz como um tuple (linha,coluna)
#
#                                       OPERADORES:             __add__                 --> + (Adição matricial)
#                                                               __mul__                 --> * (Multiplicação quer escalar quer matricial

class matriz:
    # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    # C R I A Ç Ã O      de uma      M A T R I Z            a) __init__             --> Inicialização de uma matriz nova

    # ==================================================================================
    # Função               --> __init__: Initialisa uma matriz a partir de uma lista dada, analisando
    #                             se esta é uma lista de list com as seguintes propriedades:
    #                              1) uma lista não vázia    
    #                              2) todos elementos da lista inicial são não vazias
    #                              3) tendo tudos as mesmas números de elementos
    #                           Caso contrário passamos uma matriz vazia. 
    # Entradas             --> valueList: uma lista de listas, onde as listas inteira são as linhas da matriz.
    # Saída                --> Quando 1)-3) é válido, obtemos uma matrix com os valores dado da valueList
    #
    def __init__(self, valueList):
        # verifique se a valueList é não vazio com um promeiro elemento não vazio
        if (type(valueList) != list or len(valueList) < 1 or type(valueList[0]) != list or len(valueList[0]) < 1):
            # Quando não é voltamos uma matriz vazia 
            self.value = [[]]
            return
            # verifique se os restantes elementos tenhãm o mesmo tamanho que o primeiro
        for i in range(len(valueList)):
            if (type(valueList[i]) != list or len(valueList[i]) != len(valueList[0])):
                # Quando não é voltamos uma matriz vazia
                self.value = [[]]
                return

        # Sendo ser uma matriz válida podemos copiar os valores de entrada para a matriz atual
        self.value = []
        for i in range(len(valueList)):
            line = []
            for j in range(len(valueList[0])):
                line.append(valueList[i][j])
            self.value.append(line)
        return
        # ==============================================================================

    # ==============================================================================

    # FIM da                      --->      C R I A T Ç Ã O   de uma      M A T R I Z
    # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX


    # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    # METÓDOS:                          1) showMatrix           --> Representação da matriz no ecran
    #                                   2) dim                  --> Retornar a dimensão da matriz como um tuple (linha,coluna)
    #                                   3) getValue             --> Retorna o valor da Matriz numa posição especifica

    # ===============================================================================
    # Função  --> showMatrix: Apresenta os valores da matriz linha à linha no ecran
    #
    # Entrada           --> virtual: Matriz como uma lista de listas (listas inteiras são as linhas da matriz)
    # Efeito secundário --> Apresentaçaõ no ecran dos valores da matriz linha à linha

    def showMatrix(self):
        # Apresenta no ecran todos os valores da matriz linha à linha
        for i in range(len(self.value)):
            print(self.value[i])

    # ==============================================================================

    # ==============================================================================
    # Função      --> dim: Dimensão da matriz
    #
    # Entrada     --> virtual: Matriz como uma lista de listas (listas inteiras são as linhas da matriz)
    # Saída       --> A dimensão da instância da matriz em forma de (linhas, colunas)
    #
    def dim(self):
        if (len(self.v) <= 0 or len(self.v[0]) <= 0): return (0, 0)
        return (len(self.v), len(self.v[0]))

    # ==============================================================================
    # Função      --> getValue: Retorna o valor da matriz numa posição especifica
    #
    # Entrada     --> (line,column): Indica uma posição da Matriz para retornar
    # Saída       --> Retorna o valor na posição (line,column) da Matriz
    #

    def getValue(self,line,column):
        return(self.value[line][column])

    # ==============================================================================

    # FIM do                -->                METÓDOS
    # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

    # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
    # O P E R A T O R S:    - ADIÇÃO (+)
    # -------------------------------------------------------------------------------

    # ===============================================================================
    # Função   --> __add__: OPERADOR (+) da Adição da matriz "self" com uma outra matriz "B"
    #
    # Entrada    -->  virtual: a própria matriz / matriz B para adicionar
    #
    # Saída      --> Matriz R=self+B
    #                Em caso de errors o resultado seria uma lista vázia "[[]]"
    def __add__(self, B):
        # Verifique se ambas as matrizes têm o mesmo formato
        if (len(self.value) != len(B.value) or len(self.value[0]) != len(B.value[0])):
            return (matriz([[]]))  # Não tem o mesmo formato --> retourno é a matriz vázia
        else:
            R = []  # Defina a lista externa do resultado

        # Definição da dimensão de ambas os matrizes
        lin = len(self.value)     # O númbero das linhas
        col = len(self.value[0])  # O númbero das colunas

        # Calucula a soma
        for i in range(lin):  # passa todas as linhas de A respectivamente de B
            val = []              # cria uma nova linha para a matriz resultando
            for j in range(col):  # passa todas as colunas de A respectivamente de B
                val.append(
                    self.value[i][j] + B.value[i][j])  # insere em cada coluna a soma dos elementos da mesma
            R.append(val)                              # insere a linha na matriz resultando

        return (matriz(R))  # Retourna o resultado

    # ==============================================================================
#   !!!!!!!!!!!!!!!!!!!!!! P A R A     I M P L E M E N T A R !!!!!!!!!!!!!!!!!!!!!!!
    def __mul__(self, B):
        #Verificar se len(linha A) == len(coluna B)
        if len(self.value) != len(B.value[0]):
            return matriz([[]]) #Matriz vazia se diferente
        else:
            R = [] #Definir lista para resultados

        for i in range(len(self.value)):
            total = 0
            for j in range(len(B.value[0])):
                total += self.value[i][j] * B.value[i][j]
            R.append(total)
        return (matriz(R))

        #Read first line of A, multiply value of each element in first line for each element in first column of B
        #Cycle for until it's finished
        #Return matriz


    # FIM dos                            -->  O P E R A D O R E S
    # XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

# ****************************************************************************************
# M A I N - F U N C T I O N
# ------------------------------------------------------------------    
# 1) Calculate the transposition Matrix for a camera at point (500,500,500) with the vector
#    (-150,-100,-150) on its visualization plane into to the standart projection plane of XY.
# 2) Applying this transposition also to the Cube in the origem with the length 100
# ------------------------------------------------------------------
def __main__():
    A=matriz([[1,2,3,4],[2,1,1,3],[-1,1,1,1]])
    B=matriz([[2,1,0,4],[-2,-1,-1,-3],[1,-1,-1,-1]])
    C=matriz([[12,2,0,3],[1,2,3,1],[-3,3,5,6]])
    D=matriz([[1,2,3,4],[-2,-1,-1,-3],[-1,1,1,1]])
    E=matriz([[2,1],[0,4],[1,-1],[-1,-1]])
    F=matriz([[0,3],[3,1]])
    M=D*E*F         # !!!! SUBSTITUIR POR:      M=D*E*F   !!!!!!!
    M.showMatrix()

# Executa a função __main__() quando o programa é executivel,
# mas não quando e importado
if __name__=="__main__":__main__()
    