from Matriz.transformation import *
from Matriz.matriz import matriz

# ====================================================================================
# FUNÇÃO:            readLineForLineFromFile
# PRAGMÁTICA:        Lê a informanção do ficheiro linha à linha
# ENTRADA:           ficheiro  --> Nome do ficherio que contem os dados
# SAÍDA:             table --> Tabela dos informações extraido do ficheiro
# EFEITO SEGUNDÁRIO: Mudancas nas variaveis (globais) / Efeitos (visiveis) nos dispositivos
# ---------------------------------.---------------------------------------------------
def readLineForLineFromFile(file):
    table = []                         # Preparação de uma espaço vazio para a tabela "table"
    reading = open(file, 'r')          # Criação de um gerente do ficheiro

    # Leituro do ficheiro até seu fim (acaba o ciclo indefinido com um break)
    while True:                        
        line = reading.readline()      # Leitura de uma linha do ficheiro associado com o gerente "reading"
        if not line: break             # Finaliza o ciclo quando o ficheiro está no fim
        line = line.rstrip()           # Elimine o "CR(=Carrage Return)" no fim da linha
        elements = line.split(" ")     # Faza uma lista dos elements da linha seperados com o espaço
        table.append(elements)         # Inserir a lista dos elementos da linha com uma lista na tabela "table"
    reading.close()                    # Fecha o gerente "reading"
    return (table)                     # Retorna a lista das listas "table"
# ====================================================================================

# ====================================================================================
# FUNÇÃO:            extractPoints
# PRAGMÁTICA:        Sabendo do formato do ficheiro a primeira linha contêm só um elemento
#                    que o número dos pontos que seguem. Assim extraia este pontos numa matriz
#                    com coordenadas homogéneas. Suponhamos que os pontos tem a dimensão "dim".
# ENTRADA:           table --> a tabela resultando da função readLineForLineFromFile
# SAÍDA:             matriz --> Matriz dos pontos do ficheiro
# ---------------------------------.---------------------------------------------------
def extractPoints(table,dim):
    # Caso que o formato não está corecto, retornamos uma matriz vazio
    if (len(table[0])!=1)or (len(table)<int(table[0][0])+1):return(matriz([[]]))

    points=[]
    for i in range(1,int(table[0][0])+1):
        # Caso os pontos na tabela não tem a certa dimensão, retornamos uma matriz vazio
        if len(table[i])!=dim: return (matriz([[]]))
        else: # enquando os pontos têm a dimensão certa:
            values=[]
            # Tem converter os strings da lista em inteiros
            for j in range(len(table[i])):
                values.append(int(table[i][j]))
            points.append(values) # inserir o ponto actual na lista dos pontos
            points[i-1].append(1) # converta este ponto nas coordenadas homogéneas
    # devolve a matriz com os pontos com as coordenadas homogéneas 
    return(matriz(points))             

# ====================================================================================
# FUNÇÃO:            extractPoints
# PRAGMÁTICA:        Sabendo do formato do ficheiro a primeira linha contêm só um elemento
#                    que o número dos pontos que seguem. Procura depois na linha a seguir o
#                    números dos polygonos (indexadas) nesta tabela para extrair los. 
#                    com coordenadas homogéneas. Suponhamos que os pontos tem a dimensão "dim".
# ENTRADA:           table --> a tabela resultando da função readLineForLineFromFile
# SAÍDA:             matriz --> Matriz dos pontos do ficheiro
# ---------------------------------.---------------------------------------------------
def extractPolygons(table):
    # Caso que não há informação correta sobre os pontos, retornamos uma lista vazio
    if (len(table[0])!=1 or len(table)<int(table[0][0])+1): return (matriz([[]]))
    # Caso que não há informação correta sobre os polygons, retornamos uma lista vazio
    if (len(table[int(table[0][0])+1])!=1) or (len(table)<int(table[0][0])+int(table[int(table[0][0])+1][0])+1):
        return (matriz([[]]))
    polygons=[]
    for i in range(int(table[0][0])+2,int(table[0][0])+int(table[int(table[0][0])+1][0])+2):
        # Tem converter os strings da lista em inteiros
        values=[]
        for j in range(len(table[i])):
                values.append(int(table[i][j]))
        polygons.append(values) # inserir o polígono actual na lista dos polígonos
    # devolve a matriz com os polígonos 
    return(polygons)

# ?????????????????????????????????????????????????????????????????????
def __main__():  #!!! TEM IMPLEMENTAR A FUNÇÂO PRINCIPAL
    table = readLineForLineFromFile('figure.txt') #Extrair ficheiro
    P = extractPoints(table, 2)                   #Extrair pontos do ficheir
    O = rotation(45)*translation([400,300])*rotation(30) #Transformacao dos pontos
    M = P*O
    M.showMatrix()

# Main programm
if __name__ == '__main__':__main__()


