from Matriz.matriz import matriz
import math
# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
# M O D U L :     T R A N S F O R M A T I O N          --> Cria matrizes especificas
#
#                 F U N Ç Õ E S:     CRIA TRANSFORAÇÕES:         a) translation
#                                                                b) rotation
# ==============================================================================
# Função   --> translation: Cria de uma lista com dois elementos a correspondente matriz
#                           de translação no hyper-espaço homogéneo
#
# Entrada  --> A lista com dois números considerado com um vetor plano v
# Saída    --> A corespondente matriz de translão para v no no hyper-espaço homogéneo
#              Se v não tem o formato esperado, então produz a matriz de identidade
def translation(v):
    value=[]
    # Cria a matriz da identidade em 3x3
    for i in range(0,3):
        value.append([])
        for j in range(0,3):
            if i==j: value[i].append(1)
            else: value[i].append(0)
    # Verifiqque se o vetor seja valido
    if (type(v) != list or len(v) !=2):
        # Dado o formato erado retorna a matriz de identidade em com a dimansão 3x3
        return (matriz(value))
    else:
        # Completa as duas posições faltantes na matriz da translação com os valores do vetor válido
        value[2][0]=v[0]
        value[2][1]=v[1]
        return(matriz(value))
# ==============================================================================

# ==============================================================================
# Função     --> rotation: Cria a partir do ânuglo a corespondente matriz da rotação
#
# Entrada    --> Um ângulo em radians
# Saída      --> A corespondente matriz da rotação para o ângulo dado
#                Caso que o ângulo não bate certe, retorna a matriz de identidade em 3x3
def rotation(alpha):
    # Verifique que o argumento é um valor númerico
    if (type(alpha) != float):
        # Dado do formato erado retorna a matriz da identidade
        return (matriz([[1,0,0],[0,1,0],[0,0,1]]))
        # Caso que temos um ângulo corecto, retornamos a corespondente matriz da rotação
    else:return(matriz([[math.cos(alpha),math.sin(alpha),0],[-math.sin(alpha),math.cos(alpha),0],[0,0,1]]))
# ==============================================================================


# FIM dos                -->                FUNÇÕES
# XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

# ****************************************************************************************
# M A I N - F U N C T I O N
# ------------------------------------------------------------------    
# Calculate para os pontos (0,0), (3,3), (3,0) e (0,3) a transformação quando aplicamos
# sucessivamente a) uma rotação de 45º; b) uma translação de [400,300]; c uma rotação de 30º
def __main__(): #!!!! IMPLEMENTAR a FUNÇÃO PRINCIPAL !!!!!
    A=matriz([[0,0,1],[3,3,1],[3,0,1],[0,3,1]])
    O=rotation(45)*translation([400,300])*rotation(30)
    M=A*O
    M.showMatrix()

# Executa a função __main__() quando o programa é executivel,
# mas não quando e importado
if __name__=="__main__":__main__()
    