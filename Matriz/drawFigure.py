from Matriz.transformation import *
from Matriz.matriz import matriz
from Matriz.transformFigure import *
import tkinter as TK    # Inclui a biblioteca com as funções do GUI de TKinter

# Variaveis globais para a definição da janela respetivamento do Canvas
h=800            # A altura da janela 
w=450            # O comprimento da janela


# Main program
if __name__ == '__main__':
    #Red Figure
    table=readLineForLineFromFile("figure.txt")   # Extrair o ficheiro "figure.txt
    P=extractPoints(table,2)                      # Extrai da tabela "table" os pontos
    Polygons=extractPolygons(table)               # Extrai da tabela "table" os indícies dos polígonos

    #Blue Figure
    table2=readLineForLineFromFile("figure.txt")
    V=extractPoints(table,2)*rotation(2*math.pi/8)*translation([400, 300])*rotation(2*math.pi/12)
    VPolygons=extractPolygons(table2)
    
    # Make the GUI of the program
    windows=TK.Tk()
    image=TK.Canvas(windows,cursor="arrow",bg="yellow",height=h,width=w)
    image.pack()

    #Red Figure Render
    for i in range(len(Polygons)):
        for j in range(len(Polygons[i])):
            image.create_line(P.getValue(Polygons[i][j],0),P.getValue(Polygons[i][j],1),
                              P.getValue(Polygons[i][(j+1)%len(Polygons[i])],0),
                              P.getValue(Polygons[i][(j+1)%len(Polygons[i])],1),
                              fill='red')

    #Blue Figure Render
    for i in range(len(VPolygons)):
        for j in range(len(VPolygons[i])):
            image.create_line(V.getValue(VPolygons[i][j],0),V.getValue(VPolygons[i][j],1),
                              V.getValue(VPolygons[i][(j+1)%len(VPolygons[i])],0),
                              V.getValue(VPolygons[i][(j+1)%len(VPolygons[i])],1),
                              fill='blue')
    # LOOP
    windows.mainloop()
