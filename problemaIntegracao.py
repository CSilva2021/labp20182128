import math

#Ex 1
def trapezoid(intx, amp):
    #f(X0) + f(Xn)
    trap = math.exp(-intx*amp)
    for i in range(1, intx):
        #2f(Xn-1)
        trap = trap + (2*(math.exp(-i*amp)))
    trap = (amp/2)*trap
    return trap

#Ex 2

def infected(textfile):
    f = open(textfile)
    variables = f.readlines()
    i = int(variables[0].strip())
    m = int(variables[1].strip())
    d = m/10
    if d > 0.5:
        d = d/2
    trap = (230*(math.exp(0.3*d)))+230
    for n in range(1, m):
        trap = trap + (2*(230*(math.exp(0.3*d*n))))
    trap = (d/2)*trap
    trap = trap + i
    return trap

print(trapezoid(10, 0.2))
print(infected("test.txt"))

