#from requests import get
from io import StringIO
from csv import DictReader
#import matplotlib.pyplot as plt

def constructItLines():
    url = 'https://raw.githubusercontent.com/dssg-pt//covid19pt-data/master/data_concelhos_new.csv'
    req = get(url, stream=True)
    buffer = StringIO(req.text)
    return DictReader(buffer, delimiter=',')

def dispersionData():
    keys = {"casos_14dias":[], "population_65_mais":[]}
    x = 0
    for line in constructItLines():
        if (x < 100):
            keys["casos_14dias"].append(line["casos_14dias"])
            keys["population_65_mais"].append(line["population_65_mais"])
        else:
            break
        x = x +1
    return keys


