class Empregado:
    def __init__(self, horaEspera=None):
        self.horaEspera = horaEspera

    def getHoraEspera(self):
        return self.horaEspera

    def setHoraEspera(self, hora):
        self.horaEspera = hora