import random

class Cliente:
    def __init__(self, atividade=None, horaFim=None):
        self.horaFim = horaFim
        self.atividade = atividade

    def gethoraFim(self):
        return self.horaFim

    def getAtividade(self):
        return self.atividade

    def setAtividade(self):
        active = ['Inicio', 'Fim']
        self.atividade = random.choice(active)

    def setHoraFim(self):
        self.horaFim = 0 + random.randint(0, 5)