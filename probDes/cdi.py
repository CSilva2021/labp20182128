class calculoDescritores:
    def __init__(self, horaEntrada, horaFila):
        self.horaEntrada = horaEntrada
        self.horaFila = horaFila

    def tempoEspera(self, HAS, horaEspera):
        #For PMU and TPS
        return HAS - horaEspera

    def tempoFila(self, HAS, horaFila):
        return HAS - horaFila

    def tempoEsperaTotal(self, tempo):
        # For PMU and TPS
        tempoTotal = 0
        tempoTotal += tempo
        return tempoTotal

    def tempoFilaTotal(self, tempo):
        tempoTotal = 0
        tempoTotal += tempo
        return tempoTotal

    def PMU(self, tempo, TS):
        pmu = tempo/TS
        return pmu

    def TME(self, tempo, clientNum):
        return tempo/clientNum

    def TPS(self, tempo, clientNum):
        return tempo/clientNum