from probDes.stack import Stack

class Livre:
    def __init__(self):
        self.self = Stack()

    def addEmpregado(self, empregado):
        self.self.push(empregado)

    def removeEmpregado(self):
        self.self.pop()

    def checkEmpregado(self):
        return self.self.is_empty()
