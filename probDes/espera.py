from probDes.queue import Queue
from probDes.Cliente import Cliente

class Espera:
    def __init__(self):
        self.self = Queue()

    def addClient(self, client):
        if Cliente.getAtividade() == 'fim' & Cliente.gethoraFim() <= 1:
            self.self.add(client)

    def checkClient(self):
        return self.self.is_empty()

    def removeClient(self):
        self.self.pop()

