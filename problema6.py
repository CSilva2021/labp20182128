from car import Car
import random

#Set variables

roadCars = []
stoppedCars = []
fuelLine = []
waitTime = 0

#Agents Status

def fuelPost(car):
    if car.getFuel() < car.getFuelThreshold():
        roadCars.pop(car.getId())
        fuelLine.append(car.getId())
        car.setState('fuelstation')
        car.incrementNrTimesBomb()
        car.setFuel(10)
        car.setState('onRoad')
        roadCars.append(car.getId())
        fuelLine.remove(car.getId())

def giveUp(car):
    if car.getState() == 'fuelstation':
        car.incrementWaitingTime(1)
        if car.getWaitingTime() >= car.getWaitingTimeThreshod():
            car.setState('onroad')
            fuelLine.remove(car.getId())
            roadCars.append(car.getId())

#Main Cycle

def mainCycle():
    tick=0
    agentes = dict()
    for i in range(0, 10):
        id = i
        fuel = random.randint(6, 10)
        fuelThreshold = random.randint(2, 6)
        waitingTimeThreshold = random.randint(2, 10)
        carro = Car(id, 'onroad', fuel, fuelThreshold, waitingTimeThreshold)
        agentes[id] = carro
        roadCars.append(id)
    while tick<=100:
        if len(fuelLine) != 0 and tick%2 == 0:
            check = fuelLine.pop(0)
            checkCar = agentes[check]
            checkCar.setFuel(10)
            checkCar.setState('onRoad')
            roadCars.append(check)
        c = roadCars[0]
        cCar = agentes[c]
        fuelPost(cCar)
        giveUp(cCar)
        cCar.reduceFuel(1)
        if cCar.getFuel() == 0:
            stoppedCars.append(cCar.getId())
            roadCars.remove(cCar.getId())
        tick += 1
    avgFuel = 0
    avgFuelStation = 0
    for f in range(len(agentes)):
        finalFuel = agentes[i].getFuel()
        fuelStation = agentes[i].getNrTimesBomb()
        avgFuel += finalFuel/len(agentes)
        avgFuelStation += fuelStation/len(agentes)
    print('**************')
    print('Estado Final')
    print('Nr Ticks: ' + str(tick))
    print('Nr Agentes: ' + str(len(agentes)))
    print('Estacao de servico: ' + str(len(fuelLine)))
    print('Estrada: ' + str(len(roadCars)))
    print('Parados: ' + str(len(stoppedCars)))
    print('Combustivel (Avg): ' + str((avgFuel)))
    print('Nr Vezes estacao (Avg): ' + str(avgFuelStation))
    print('**************')


def main():
    mainCycle()

main()
