#Caesar Cipher Encryption
#Ex1
def caesarCipher(textfile, key: int) -> str:
    f = open(textfile)
    chars = f.read()
    encrypt = ''
    for c in range(len(chars)):
        char = chars[c]
        #Encrypt lowercase using encryption (x + n) mod 26
        if (char >= 'a' and char <= 'z'):
            encrypt += chr((ord(char)+ key-97)%26 + 97)
        else:
            #Leave spaces as is
            encrypt += char
    return encrypt

#Ex2
def decrypt(textfile, key: int) -> str:
    f = open(textfile)
    chars = f.read()
    decryption = ''
    for c in range(len(chars)):
        char = chars[c]
        # Decrypt lowercase using (x - n) mod 26
        if (char >= 'a' and char <= 'z'):
            decryption += chr((ord(char) - key - 97) % 26 + 97)
        else:
            # Leave spaces as is
            decryption += char
    return decryption

#Ex3
def severalKeys(textfile, keylist: list) -> str:
    f = open(textfile)
    chars = f.read()
    encrypt = ''
    keys = []
    for k in range(len(chars)):
        keys.append(keylist[0])
        keys.append(keylist[1])
    for c in range(len(chars)):
        char = chars[c]
        key = keys[c]
        # Encrypt lowercase using encryption (x + n) mod 26
        if (char >= 'a' and char <= 'z'):
            encrypt += chr((ord(char) + key - 97) % 26 + 97)
        else:
            # Leave spaces as is
            encrypt += char
    return encrypt

def decryptKeys(textfile, keylist: list) -> str:
    f = open(textfile)
    chars = f.read()
    encrypt = ''
    keys = []
    for k in range(len(chars)):
        keys.append(keylist[0])
        keys.append(keylist[1])
    for c in range(len(chars)):
        char = chars[c]
        key = keys[c]
        # Decrypt lowercase using encryption (x + n) mod 26
        if (char >= 'a' and char <= 'z'):
            encrypt += chr((ord(char) - key - 97) % 26 + 97)
        else:
            # Leave spaces as is
            encrypt += char
    return encrypt

def main():
    print(open('test.txt').read())
    print(severalKeys('test.txt', [3,5]))
    print(decryptKeys('test2.txt', [3,5]))

main()




